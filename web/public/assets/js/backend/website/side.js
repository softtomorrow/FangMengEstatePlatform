define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            Table.api.init({
                extend: {
                    index_url: 'website/side/index',
                    add_url: 'website/side/add',
                    edit_url: 'website/side/edit',
                    del_url: 'website/side/del',
                    multi_url: '',
                    table: '',
                }
            });

            var table = $("#table");
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
						{field: 'state', checkbox: true,},
                        {field: 'id', title: __('Id')},
						{field: 'title', title: __('title'), operate: 'LIKE %...%', placeholder: '模糊搜索，*表示任意字符'},
						{field: 'status', title: __('Status'), searchList: {"0": "正常", "1": "隐藏"}, formatter: Table.api.formatter.status},
						{
                            field: 'createtime',
                            title: __('Release time'),
                            sortable: true,
                            formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'
                        },
						{field: 'image', title: __('picture'), operate: false, formatter: Table.api.formatter.image},
                        {
                            field: 'operate',
                            width: "130px",
                            title: __('operation'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        },
                    ]
                ],
                search: true,
                showExport: false,
                searchFormVisible: false,

            });

            Table.api.bindevent(table);
			var url = '';
			$(".btn-add").off("click").on("click", function () {
                var url = 'website/side/add';
				Fast.api.open(url, __('Add'), $(this).data() || {});
                return false;
            });
			
			$(document).on("click", ".btn-start,.btn-pause", function () {
                var ids = Table.api.selectedids(table);
                Table.api.multi("changestatus", ids.join(","), table, this);
            });
			
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});