define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'area/items/index' + location.search,
                    add_url: 'area/items/add',
                    edit_url: 'area/items/edit',
                    del_url: 'area/items/del',
                    multi_url: 'area/items/multi',
                    table: 'area_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'areaId',
                sortName: 'areaId',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'areaId', title: __('Areaid')},
                        {field: 'areaCode', title: __('Areacode')},
                        {field: 'areaName', title: __('Areaname')},
                        {field: 'level', title: __('Level')},
                        {field: 'cityCode', title: __('Citycode')},
                        {field: 'center', title: __('Center')},
                        {field: 'parentId', title: __('Parentid')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});