define(['jquery', 'bootstrap', 'backend', 'table', 'form','backend/maputils','backend/second/househelper','backend/second/inspects','backend/second/texthelper','backend/stringutils'],
 function ($, undefined, Backend, Table, Form,map,helper,inspectsfunc,texthelper) {
    var arr=['在租','停租','已租','无效'];
    var labels=['success','primary','info','default'];
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'rent/items/index' + location.search,
                    add_url: 'rent/items/add',
                    edit_url: 'rent/items/edit',
                    del_url: 'rent/items/del',
                    multi_url: 'rent/items/multi',
                    table: 'rent_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),formatter:function(value,row,index){
                            var str=value;
                            if(row.view_level>0){
                                str += " <span class='label label-warning'>私</span>";
                            }else{
                                str += " <span class='label label-default'>公</span>";
                            }
                            return str;
                        }},
                        {field: 'newhousebuilding.areaName', title:'区域',operate: 'LIKE',formatter:function(c,r){
                            if(c){
                                var _array = c.split('/');
                                return _array[_array.length-1];
                            }else{
                                return "&nbsp;";
                            }
                        }},
                        {field: 'newhousebuilding.building_name', title:'小区名称',operate: 'LIKE',formatter:function(value,row){
                            if(second_show_fanghao){
                                if(!value) value='楼盘字典异常';
                                var str = value;
                                if(row.dong) str += ' ' + row.dong;
                                if(row.shi)  str += '#' + row.shi;
                                return str;
                            }else{
                                return value;
                            }
                        }},
                        {field: 'dong', title: __('Dong'),operate: 'LIKE',visible:false},
                        {field: 'shi', title: __('Shi'),operate: 'LIKE',visible:false},
                        {field: 'shi_count', title:'房型',formatter:function(c,r){
                            return c + '室' + r.ting_count + '厅';
                        }},
                        {field: 'floor', title: __('Floor'),formatter:function(c,r){
                            return c + '/' + r.floor_total;
                        }},
                        {field: 'area', title: __('Area'), operate:'BETWEEN',formatter:function(c,r){
                            return c + ' m<sup>2</sup>';
                        }},
                        {field: 'unit_price', title:'元/月', operate:'BETWEEN'},
                        {field: 'deposit', title: __('Deposit'), operate:'BETWEEN',formatter:function(c,r){
                            return c + '元';
                        }},
                        {field: 'customer_name', title: __('Customer_name'),operate: 'LIKE'},
                        {field: 'mobile', title: __('mobile'),operate: 'LIKE',visible:false},
                        {field: 'seconddecoration.decoration', title: __('Seconddecoration.decoration')},
                        {field: 'newhousebuildingtypes.type_name', title:'类型'},
                        {field: 'state', title:'状态',searchList: {"0":('在售'),"1":'停售',"2":'已售','3':'无效'},formatter:function(value,row,index){
                            var str= "<span class='label label-" + labels[value] + "'>" + arr[value] + "</span>";
                            return str;
                        }},
                        {field: secondSort, title: secondSortName, operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'admin.nickname', title: '录入',operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name:'view',
                                text:'查看',
                                title:'查看',
                                icon:'fa fa-building',
                                classname:'btn btn-xs btn-success btn-view btn-dialog',
                                url:'rent/items/view'
                            }
                        ],formatter: Table.api.formatter.operate
                    }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $("a.btn-view").data('area',["90%","95%"]);
                $("a.btn-editone").data('area',["85%","85%"]);
            });
            table.on('dbl-click-row.bs.table', function (e, row, element, field) {
                $(".btn-view", element).trigger("click");
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        /**
         * 判断房源是否存在
         */
        checkExistsDongShi:function(){
            $("#ulDuplicateDongShi").empty();
            if($("#c-building_id").val() && $("#c-dong").val() &&  $("#c-shi").val()){
                $.post('rent/items/checkExistsDongShi',{building_id:$("#c-building_id").val(),dong:$("#c-dong").val(),shi:$("#c-shi").val()},function(r){
                    if(r.exists){
                        Toastr.error('房源已经存在');
                        var str= ' <span >编号 : ' + r.id + '</span> ';
                        str += ' <span>号码:' + r.mobile + '</span> <a href="rent/items/view/ids/' + r.id 
                        + '" class="btn-view btn-dialog" title="查看" data-table-id="table" data-field-index="17" data-row-index="0" data-button-index="0">' 
                        + ' <i class="fa fa-building"></i> 查看 </a>';
                        $("#ulDuplicateDongShi").append('<li class="list-group-item">' + str + '</li>');
                    }else if(r.msg){
                        Toastr.error('没有权限检查房源是否存在')
                    }
                    else{
                        $("#ulDuplicateDongShi").append('<li class="list-group-item">没有重复房源,允许录入</li>');
                    }
                });
            }
            // 如果所在层没有值，就自动填充
            var _shi = $("#c-shi").val();
            if(_shi && !$("#c-floor").val()){
                if(_shi.length>=3 && _shi.length<=4){
                    var _floor = _shi.substring(0,_shi.length-2);
                    $("#c-floor").val(_floor);
                }
            }
            // 如果没有总层数，从服务器读取
            if($("#c-dong").val() && $("#c-building_id").val() && !$("#c-floor_total").val() ){
                $.get('second/items/getFloorTotal',{building_id:$("#c-building_id").val(),dong:$("#c-dong").val()},function(retFloor){
                    $("#c-floor_total").val(retFloor.floorTotal);
                });
            }
        },
        checkExistsMobile:function(){
            $("#ulDuplicateMobile").empty();
            if($("#c-mobile").val()){
                $.post('rent/items/checkExistsMobile',{mobile:$("#c-mobile").val()},function(r){
                    if(r.exists){
                        Toastr.error('该号码登记过房源');
                        var str='';
                        for(var i in r.ids){
                            str += "<li class='list-group-item'>";
                            str += ' <span >编号 : ' + r.ids[i].id + '</span>';
                            str += ' <span>' + r.ids[i].building_name + '</span>';
                            str += ' <span>' + r.ids[i].dong + '</span>#<span>' + r.ids[i].shi + '</span>';
                            str += ' <a href="rent/items/view/ids/' 
                                + r.ids[i].id 
                                + '" class="btn-view btn-dialog" title="查看" data-table-id="table" data-field-index="17" data-row-index="0" data-button-index="0">' 
                                + ' <i class="fa fa-building"></i> 查看 </a>';
                            str += "</li>";
                        }
                        
                        $("#ulDuplicateMobile").append(str);
                    }else if(r.msg){
                        Toastr.error('没有权限检查房源是否存在')
                    }else{
                        $("#ulDuplicateMobile").append('<li class="list-group-item">没有重复房源</li>');
                    }
                });
            }
        },
        view:function(){
            var html="<span class='label label-" + labels[state] + "'>" + arr[state] + "</span>";
            $("#tdState").html(html);
            //TODO: 地址
            if(lat && lng){
                map.init("map-container",lng,lat,function(e){
                    map.getLocation({lat:lat,lng:lng},function(result){
                        if(result){
                            $("#c-address").text(result.address); 
                        }
                    });
                });
            }else{
                map.init("map-container",117.17,31.52,function(e){
                    map.geo(building,function(point,ret){
                            
                    });
                });
               
            }
            renderFollow();
         
            function renderFollow(){
                console.info('加载跟进');
                // 加载跟进
                $.post('rent/follow',{item_id:item_id}, function(data, ret){
                    //成功的回调
                    var value=data.rows;
                    $("#ulFollow").empty();
                    var state;
                    for(var i=0;i<value.length;i++){
                        var tmp=value[i];
                        if(i==0)state=tmp.state;
                        var str='<li>[' + tmp.create_time + ']';
                        str +='[ '+arr[ tmp.state] + ' ]';
                        str += tmp.comment;
                        str += ' [ ' + tmp.admin.nickname + ' ] ';
                        str+='</li>';
                        $("#ulFollow").append(str);
                    }
                    if(state){
                        var html="<span class='label label-" + labels[state] + "'>" + arr[state] + "</span>";
                        $("#tdState").html(html);
                    }
                    return false;
                 });
            }
            $("#btnNewFollow").click(function(){
                Fast.api.open("rent/follow/add?item_id=" + item_id + '&state=' + state, '新增跟进', {
                    callback: function (data) {
                        renderFollow();
                    }
                });
                return false;
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});