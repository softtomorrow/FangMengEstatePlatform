define(['jquery', 'bootstrap', 'backend', 'table', 'form','../../../../libs/bootstrap-colorpicker/js/bootstrap-colorpicker'], function ($, undefined, Backend, Table, Form,colorpicker) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'newhouse/building/labels/index' + location.search,
                    add_url: 'newhouse/building/labels/add',
                    edit_url: 'newhouse/building/labels/edit',
                    del_url: 'newhouse/building/labels/del',
                    multi_url: 'newhouse/building/labels/multi',
                    table: 'newhouse_building_labels',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'label_name', title: __('Label_name')},
                        {field: 'label_color', title: __('Label_color')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            $(function () {
                $('#c-label_color').colorpicker();
           });
        },
        edit: function () {
            Controller.api.bindevent();
            $(function () {
                 $('#c-label_color').colorpicker();
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});