define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'newhouse/building/follow/index' + location.search,
                    del_url: 'newhouse/building/follow/del',
                    table: 'newhouse_building_follow',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'newhousebuildingitems.building_name', title: __('Newhousebuildingitems.building_name')},
                        {field: 'comment', title: __('Comment')},
                        {field: 'state', title: __('State'), searchList: {"待售":__('待售'),"停售":__('停售'),"在售":__('在售'),"售罄":__('售罄')}, formatter: Table.api.formatter.normal},
                        {field: 'admin.username', title: __('Admin.username')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            // Controller.api.bindevent();
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                //给表单绑定新的回调函数 接收 控制器 success(msg,url,data)或者error(msg,url,data)
                Fast.api.close(data);//在这里
                console.log("成功",data);
            }, function(data, ret){
                console.error("错误");
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});