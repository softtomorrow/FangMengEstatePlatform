define(['jquery', 'bootstrap', 'backend', 'table', 'form','slimscroll','moment'], 
function ($, undefined, Backend, Table, Form,slimScroll,moment) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'newhouse/customer/items/index' + location.search,
                    add_url: 'newhouse/customer/items/add',
                    edit_url: 'newhouse/customer/items/edit',
                    multi_url: 'newhouse/customer/items/multi',
                    table: 'newhouse_customer_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'mf_newhouse_customer_items.id',
                dblClickToEdit:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'customer_name', title: __('Customer_name'),operate: 'LIKE'},
                        {field: 'customer_tel', title: __('Customer_tel'),operate: 'LIKE'},
                        {field: 'want_area', title: __('Want_area'),visible:false},
                        {field: 'sex_radio', title: __('Sex_radio'),  formatter: Table.api.formatter.normal},
                        {field: 'first_house', title: __('First_house'),visible:false, searchList: {"是":__('是'),"否":__('否'),"未知":__('未知')}, formatter: Table.api.formatter.normal},
                        {field: 'social_security', title: __('Social_security'), searchList: {"有":__('有'),"无":__('无'),"未知":__('未知')}, formatter: Table.api.formatter.normal},
                        {field: 'payment_method', title: __('Payment_method'), searchList: {"现金":__('现金'),"刷卡":__('刷卡'),"支付宝":__('支付宝'),"微信":__('微信')}, formatter: Table.api.formatter.normal},
                        {field: 'payment_percentage', title: __('Payment_percentage')},
                   
                        {field: 'customer_from', title: __('Customer_from')},
                        {field:'state',title:'跟进状态',formatter:function(value,row){
                            var labels={'带看':'success','有效':'success','观望':'primary','不确定':'info','已购':'info','无效':'default'};
                            return "<span class='label label-" + labels[value] + "'>" + value + "</span>";
                        }},
                        {field:'admin.nickname',title:'录入人'},
                        {field: 'mf_newhouse_customer_items.createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: function(value,row){
                            return Table.api.formatter.datetime(row.createtime);
                        }},
                        {field: 'mf_newhouse_customer_items.updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: function(value,row,column){
                            return Table.api.formatter.datetime(row.updatetime);
                        }},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons:[
                            {
                                name: 'view',
                                title: __('查看客户'),
                                classname: 'btn btn-xs btn-primary btn-dialog btn-view',
                                icon: 'fa fa-tablet',
                                url: 'newhouse/customer/items/view',
                                callback: function (data) {
                                    Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });
            table.on('post-body.bs.table',function(){
                $("a.btn-view").data('area',["1000px","600px"]);
            });
            table.on('dbl-click-row.bs.table', function (e, row, element, field) {
                $(".btn-view", element).trigger("click");
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $("#c-customer_tel").change(function(){
                $.get('newhouse/customer/items/querytel',{tel:$(this).val()},function(ret){
                    if(ret.exists){
                        Toastr.error('该客户电话在系统中已经录入');
                        $("#c-customer_tel").parent().addClass('has-warning');
                    }else{
                        $("#c-customer_tel").parent().removeClass('has-warning');
                    }
                });
            });
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        view:function(){
            $("#ulFollow ul").slimScroll({
                height: '360px'
            });
            $("#ulFollow").removeClass('hide');
            $("#btnFollowAddState").click(function(){
                Fast.api.open('newhouse/customer/follow/addstate/ids/' + customer_id,'跟进状态',{
                    callback:function(data){
                        var str="<li class='list-group-item'>[" + data.create_time + "][" + data.state + "][" + data.create_user + "]" +  data.follow_content + "</li>";
                        $("#ulFollow ul").append(str);
                    }
                });
                return false;
            });
            $("#btnFollowAddMobile").click(function(){
                parent.Fast.api.open('newhouse/customer/follow/addmobile/ids/' + customer_id,'跟进号码',{
                    callback:function(data){
                        if(!$("#tdTel2").text())
                            $("#tdTel2").text(data.mobile);
                        else if(!$("#tdTel3").text())
                            $("#tdTel3").text(data.mobile);
                        else{
                            return;
                        }
                        // var str="<li class='list-group-item'>[" + data.create_time + "][" + data.state + "][" + data.create_user + "]" +  data.follow_content + "</li>";
                        // $("#ulFollow ul").append(str);
                    }
                });
                return false;
            });
 
            // 报备
            $("#btnReportAdd").click(function(){
                $.post('newhouse/report/items/add_ajax',{
                    customer_id:customer_id,
                    prepare_visit:$("input#c-prepare_visit").val(),
                    contents:$("textarea#report-contents").val(),
                    building_id:$("#c-building_id").val()
                },function(ret){
                    if(ret.success){
                        var building_name=$("#c-building_id").selectPageText();
                        var str="<tr><td>" + building_name + "</td><td>" + moment().format('L') + "</td><td>未审核</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
                        $("#tblBuildings").append(str);
                        Toastr.success('楼盘报备成功，请等待审核结果。');
                    }
                    else{
                        Toastr.error('楼盘已经报备过了');
                    }
                });
                return false;
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});