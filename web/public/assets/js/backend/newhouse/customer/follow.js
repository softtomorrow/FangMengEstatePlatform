define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'newhouse/customer/follow/index' + location.search,
                    del_url: 'newhouse/customer/follow/del',
                    table: 'newhouse_customer_follow',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'newhousecustomeritems.customer_name', title: __('Newhousecustomeritems.customer_name')},
                        {field: 'newhouseadmin.nickname', title: __('Create_userid')},
                        {field: 'state', title: __('State'), searchList: {"无效":__('无效'),"带看":__('带看'),"观望":__('观望'),"已购":__('已购')}, formatter: Table.api.formatter.normal},
                        {field: 'follow_content', title: __('Follow_content')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange',formatter:Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        addstate: function () {
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                Fast.api.close(data);
            }, function(data, ret){
                Toastr.success("失败");
            });
        },
        addmobile: function () {
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                Fast.api.close(data);
            }, function(data, ret){
                Toastr.success("失败");
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});