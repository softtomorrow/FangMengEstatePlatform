define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'newhouse/report/items/index' + location.search,
                    add_url: 'newhouse/report/items/add',
                    edit_url: 'newhouse/report/items/edit',
                    del_url: 'newhouse/report/items/del',
                    multi_url: 'newhouse/report/items/multi',
                    table: 'newhouse_report_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'newhousecustomer.customer_name', title: __('Newhousecustomer.customer_name')},
                        {field: 'newhousecustomer.customer_tel', title: __('Newhousecustomer.customer_tel')},
                        {field: 'newhousecustomer.sex_radio', title: __('Newhousecustomer.sex_radio'), searchList: {"男":__('男'),"女":__('女'),"未知":__('未知')}, formatter: Table.api.formatter.normal},
                        {field: 'newhousecustomer.first_house',visible:false,  title: __('Newhousecustomer.First_house'), searchList: {"是":__('是'),"否":__('否'),"未知":__('未知')}, formatter: Table.api.formatter.normal},
                        {field: 'newhousecustomer.want_area', title: __('Newhousecustomer.Want_area'),visible:false},
                        {field: 'newhousebuilding.building_name', title: __('Newhousebuilding.building_name')},
                        {field: 'prepare_visit',title:'预计看房时间',operate:'RANGE',addclass:'datetimerange',formatter:Table.api.formatter.datetime},
                        {field: 'newhousecreateadmin.nickname', title: '报备人'},
                        {field: 'newhousecreateadmin.parent_nickname',visible:false, title: '报备人组织'},
                        {field: 'createtime', title: '报备时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'verify_state', title: '审核状态'},
                        {field: 'verify_friend', title: '老带新审核',formatter:function(v){
                            return v?"有效":"无效";
                        }},
                        {field: 'newhouseverifyadmin.nickname', title: '审核人'},
                        {field: 'verify_time', title: '审核时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons:[
                            {
                                name: 'view',
                                title: __('审核'),
                                classname: 'btn btn-xs btn-primary btn-dialog btn-view',
                                icon: 'fa fa-check',
                                url: 'newhouse/report/items/view'
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });
            table.on('post-body.bs.table',function(){
                $("a.btn-view").data('area',["1000px","670px"]);
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        view:function(){
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                 Toastr.success("审核操作成功");

            }, function(data, ret){
                 Toastr.success("审核操作失败");
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});