define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'crm/cop/index' + location.search,
                    add_url: 'crm/cop/add',
                    edit_url: 'crm/cop/edit',
                    del_url: 'crm/cop/del',
                    multi_url: 'crm/cop/multi',
                    table: 'crm_cop',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'cop_id',
                sortName: 'cop_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'cop_id', title: __('Cop_id')},
                        {field: 'cop_name', title: __('Cop_name')},
                        {field: 'manage_name', title: __('Manage_name')},
                        {field: 'tel', title: __('Tel')},
                        {field: 'legal_person', title: __('Legal_person')},
                        {field: 'address', title: __('Address')},
                        {field: 'latitude', title: __('Latitude'), operate:'BETWEEN'},
                        {field: 'longitude', title: __('Longitude'), operate:'BETWEEN'},
                        {field: 'state', title: __('State'), searchList: {"可用":__('可用'),"不可用":__('不可用')}, formatter: Table.api.formatter.normal},
                        {field: 'count', title: __('Count')},
                        {field: 'add_user_id', title: __('Add_user_id')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'add_ip', title: __('Add_ip')},
                        {field: 'last_time', title: __('Last_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'last_ip', title: __('Last_ip')},
                        {field: 'nvr_count', title: __('Nvr_count')},
                        {field: 'orders', title: __('Orders')},
                        {field: 'region_id', title: __('Region_id')},
                        {field: 'crmcopregions.region_name', title: __('Crmcopregions.region_name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});