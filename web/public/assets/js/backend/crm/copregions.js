define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'crm/copregions/index' + location.search,
                    add_url: 'crm/copregions/add',
                    edit_url: 'crm/copregions/edit',
                    del_url: 'crm/copregions/del',
                    multi_url: 'crm/copregions/multi',
                    table: 'crm_copregions',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'region_id',
                sortName: 'region_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'region_id', title: __('Region_id')},
                        {field: 'region_name', title: __('Region_name')},
                        {field: 'add_user_id', title: __('Add_user_id')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'add_ip', title: __('Add_ip')},
                        {field: 'last_user_id', title: __('Last_user_id')},
                        {field: 'last_time', title: __('Last_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'last_ip', title: __('Last_ip')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});