define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'assessment/templates/index' + location.search,
                    add_url: 'assessment/templates/add',
                    edit_url: 'assessment/templates/edit',
                    del_url: 'assessment/templates/del',
                    multi_url: 'assessment/templates/multi',
                    table: 'assessment_templates',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'template_typeid', title: __('Template_typeid')},
                        {field: 'cop_id', title: __('Cop_id')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'create_userid', title: __('Create_userid')},
                        {field: 'create_ip', title: __('Create_ip')},
                        {field: 'last_time', title: __('Last_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'last_userid', title: __('Last_userid')},
                        {field: 'last_ip', title: __('Last_ip')},
                        {field: 'authcops.cop_name', title: __('Authcops.cop_name')},
                        {field: 'assessmenttemplatetype.template_type', title: __('Assessmenttemplatetype.template_type')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});