define(['jquery', 'bootstrap', 'backend', 'table', 'form','backend/maputils'], function ($, undefined, Backend, Table, Form,map) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'assessment/items/index' + location.search,
                    add_url: 'assessment/items/add',
                    edit_url: 'assessment/items/edit',
                    del_url: 'assessment/items/del',
                    multi_url: 'assessment/items/multi',
                    table: 'assessment_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'address', title: __('Address')},
                        {field: 'obligee', title: __('Obligee')},
                        {field: 'area', title: __('Area'), operate:'BETWEEN'},
                        {field: 'create_time', title: '提交时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'need_report',title:'评估类型',formatter:function(value,row){
                            var str='';
                            if(row.need_prereport){
                                str +='预评';
                            }
                            if(row.need_report){
                                str += '正评';
                            }
                            return str;
                        }},
                        {field: 'state', title: '状态'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        noprereport: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'assessment/items/noprereport' + location.search,
                    add_url: 'assessment/items/add',
                    edit_url: 'assessment/items/edit',
                    del_url: 'assessment/items/del',
                    multi_url: 'assessment/items/multi',
                    table: 'assessment_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'address', title: __('Address')},
                        {field: 'obligee', title: __('Obligee')},
                        {field: 'area', title: __('Area'), operate:'BETWEEN',formatter:function(value){
                            return value + ' 平米';
                        }},
                        {field: 'create_time', title: '提交时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'need_report',title:'评估类型',formatter:function(value,row){
                            var str='';
                            if(row.need_prereport){
                                str +='预评 ';
                            }
                            if(row.need_report){
                                str += '正评';
                            }
                            return str;
                        }},
                        {field: 'state', title: __('state'),formatter:function(cell){
                            return cell==0?"无效":"有效";
                        }},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,
                            buttons:[{
                                    name:'view',
                                    text:'预评',
                                    title:'预评',
                                    icon:'fa fa-bus',
                                    classname:'btn btn-xs btn-success btn-view btn-dialog btn-prereport-dialog',
                                    url:'assessment/prereport/add1'
                                }
                                // ,
                                // {
                                //     name:'view',
                                //     text:'正评',
                                //     title:'正评',
                                //     icon:'fa fa-car',
                                //     classname:'btn btn-xs btn-success btn-view btn-dialog',
                                //     url:'assessment/report/add'
                                // }
                            ]}
                    ]
                ] 
                ,onPostBody:function(arg1){
                    console.log('onpostbody');
                    $(".btn-editone").data('area',['95%','95%']);
                    $(".btn-prereport-dialog").data('area',['95%','95%']);
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        prereport_notverify: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'assessment/items/prereport_notverify' + location.search,
                    edit_url: 'assessment/items/edit',
                    del_url: 'assessment/items/del',
                    multi_url: 'assessment/items/multi',
                    table: 'assessment_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'address', title: __('Address')},
                        {field: 'obligee', title: __('Obligee')},
                        {field: 'area', title: __('Area'), operate:'BETWEEN'},
                        {field: 'create_time', title: '提交时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
                ,onPostBody:function(arg1){
                    console.log('onpostbody');
                    $(".btn-editone").data('area',['95%','95%']);
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        prereport_verified: function () {
            console.log(1);
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'assessment/items/prereport_verified' + location.search,
                    edit_url: 'assessment/items/edit',
                    del_url: 'assessment/items/del',
                    multi_url: 'assessment/items/multi',
                    table: 'assessment_items',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'id', title: __('Id')},
                        {field: 'prereport_no',title:'预评编号'},
                        {field: 'address', title: __('Address'),operate:'LIKE'},
                        {field: 'obligee', title: __('Obligee'),operate:'LIKE'},
                        {field: 'area', title: __('Area'), operate:'BETWEEN'},
                        // {field: 'create_time', title: '提交时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'prereport_verify_time', title: '审核时间', operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'prereportadmin.username', title: '审核人',operate:'LIKE'},
                        {field: 'prereport_state_text',title:'审核结果',formatter:function(value){
                            if(value && value=='审核通过') return "<span class='label label-success'>"+value + "</span>";
                            else return "<span class='label label-default'>"+value + "</span>";
                        }},
                        {field: 'prereportadmin.username',title:'评估师',operate:'LIKE'},
                        {field: 'document_from',title:'来源',operate:'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                         formatter: Table.api.formatter.operate,buttons:[
                            {
                                name: 'detail',
                                text: '预览',
                                title: '预览',
                                icon: 'fa fa-list',
                                classname: 'btn btn-xs btn-success btn-dialog',
                                url:'/admin2020.php/assessment/prereport/previewpdf'
                            },
                            {
                                name: 'detail',
                                text: '下载',
                                title: '下载',
                                icon: 'fa fa-download',
                                classname: 'btn btn-xs btn-success',
                                url:'/admin2020.php/assessment/prereport/downloadpdf'
                            }
                         ]}
                    ]
                ],
                onPostBody:function(arg1){
                    console.log('onpostbody');
                    $(".btn-editone").data('area',['90%','90%']);
                }
            });
            // table.on('post-body.bs.table',function(){
            //     console.log(111);
            // });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $("input#checkbox-need_report,input#checkbox-need_prereport").change(function(){
                var target = $(this).data("effect");
                if($(this).prop("checked")){
                    $(target).val(1);
                }else{
                    $(target).val(0);
                }
            });
            map.init("cop-map-container",117.282699,31.866842,function(e){
                $("input#c-longitude").val(e.point.lng);
                $("input#c-latitude").val(e.point.lat);
                map.getLocation(e.point,function(result){
                    if(result){
                        $("#c-address").val(result.address); 
                        //TODO: areacode赋值 result.addressComponents.province city district streat streetNumber
                        // $("#c-business").val(result.business);
                    }
                });
            });

            $("input#btn-map").click(function(){
                map.geo($("#c-address").val(),function(point,ret){
                    $("input#c-latitude").val(point.lat);
                    $("input#c-longitude").val(point.lng);
                    // $("#c-business").val(ret.business);
                });
            });
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
            $("input#checkbox-need_report,input#checkbox-need_prereport").change(function(){
                var target = $(this).data("effect");
                if($(this).prop("checked")){
                    $(target).val(1);
                }else{
                    $(target).val(0);
                }
            });
            map.init("cop-map-container",longitude,latitude,function(e,result){
                $("input#c-longitude").val(e.point.lng);
                $("input#c-latitude").val(e.point.lat);
                if(result){
                    $("#c-address").val(result.address); 
                    //TODO: areacode赋值 result.addressComponents.province city district streat streetNumber
                    // $("#c-business").val(result.business);
                }
            });

            $("input#btn-map").click(function(){
                map.geo($("#c-address").val(),function(point,ret){
                    $("input#c-latitude").val(point.lat);
                    $("input#c-longitude").val(point.lng);
                });
            });
            $("button[name=btn-prereport]").click(function(){
                var area = [ '90%', '90%'];

                parent.Fast.api.open('assessment/prereport/add1/ids/' + _ids + '?dialog=1&itemid='+_ids,'预评报告',{
                    area:area,
                    callback:function(value){
                        console.log(value);
                    }
                });
            });
            /**
             * 下载预评报告
             */
            $("button[name=btn-prereport-download]").click(function(){
                window.open('/admin2020.php/assessment/prereport/downloadpdf?ids=' + _ids);
            });
            /**
             * 预览
             */
            $("button[name=btn-prereport-preview]").click(function(){
                window.open('/admin2020.php/assessment/prereport/previewpdf?ids=' + _ids);
            });
            $("button#btn-map").click(function(){
                // map.position()
                map.position(31.866842,117.282699);
            });
            $("button[name=btn-prereport-verify]").click(function(){
                var verify_state = $(this).attr("data-value");
                $.post('assessment/prereport/verify',{ids:_ids, state:verify_state},function(ret){
                    console.log(ret);
                    if(ret.success){
                        Toastr.success("审核完成");
                        
                        var state_class=['label-primary','label-info','label-success','label-warning'];
                        var state_txt  =['未预评','已预评','审核通过','审核拒绝']
                        var html='<h5><span class="label ' + state_class[verify_state] + '">' + state_txt[verify_state] + '</span></h5>';
                        $("#div-prereport-state").html(html);
                        

                    }else{
                        Toastr.error("审核时服务器出现故障");
                    }
                });
            });
           
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});