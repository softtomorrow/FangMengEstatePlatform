define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cert/loan/follow/index' + location.search,
                    add_url: 'cert/loan/follow/add',
                    edit_url: 'cert/loan/follow/edit',
                    del_url: 'cert/loan/follow/del',
                    multi_url: 'cert/loan/follow/multi',
                    table: 'cert_loan_follow',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'follow_id',
                sortName: 'follow_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'follow_id', title: __('Follow_id')},
                        {field: 'item_id', title:__('Item_id')},
                        {field: 'admin.nickname', title: __('Admin.nickname')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'state', title: __('State')},
                        {field: 'certloanstate.state_name', title: __('Certloanstate.state_name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    Fast.api.close(data);
                }, function(data, ret){
                    console.error(data,ret);
                    Toastr.error('跟进操作失败');
                });
            }
        }
    };
    return Controller;
});