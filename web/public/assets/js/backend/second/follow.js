define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var arr=['在售','停售','已售','无效'];
    var labels=['success','primary','info','default'];
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'second/follow/index' + location.search,
                    table: 'second_follow',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'item_id',
                sortName: 'id',
                columns: [
                    [
                        {field: 'item_id', title: __('Item_id')},
                        {field: 'seconditems.address', title: __('Seconditems.address')},
                        {field: 'comment', title: __('Comment')},
                        {field: 'state', title: __('State'),formatter:function(value){
                            return "<span class='label label-" + labels[value] + "'>" + arr[value] + "</span>";
                        }},
                        {field: 'admin.nickname', title: '跟进人'},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name:'view',
                                text:'查看',
                                title:'查看',
                                icon:'fa fa-building',
                                classname:'btn btn-xs btn-success btn-view btn-dialog',
                                url:'second/items/view'
                            }
                        ],formatter: Table.api.formatter.operate
                    }
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $("a.btn-view").data('area',["90%","95%"]);
                $("a.btn-editone").data('area',["85%","85%"]);
            });
            table.on('dbl-click-row.bs.table', function (e, row, element, field) {
                $(".btn-view", element).trigger("click");
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    Fast.api.close(data);
                }, function(data, ret){
                    console.error(data,ret);
                    Toastr.error('跟进操作失败');
                });
            }
        }
    };
    return Controller;
});