<?php

namespace app\common\model;

use think\Cache;
use think\Model;

/**
 * 二手房源
 */
class SecondItems extends Model
{
    // 表名,不含前缀
    protected $name = 'second_items';
    // 定义时间戳字段名
    protected $createTime = 'add_time';
    protected $updateTime = 'update_time';

          // 追加属性
    protected $append = [
        'state_text'
    ];
    public function getStateList()
    {
        return ['0' => __('在售'), '1' => __('停售'), '2' => __('已售'), '3' => __('无效')];
    }
    public function getStateTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['state']) ? $data['state'] : '');
        $list = $this->getStateList();
        return isset($list[$value]) ? $list[$value] : '';
    }
    
    public function seconddecoration()
    {
        return $this->belongsTo('app\admin\model\second\Decoration', 'decoration_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function authcops()
    {
        return $this->belongsTo('app\admin\model\auth\Cops', 'add_cop_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function newhousebuildingtypes()
    {
        return $this->belongsTo('app\admin\model\newhouse\building\Types', 'type_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function newhousebuilding(){
        return $this->belongsTo('app\admin\model\second\Buildings','building_id','id',[],'LEFT')->setEagerlyType(0);
    }

    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'add_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function updateadmin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'update_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }


}