<?php

namespace app\admin\controller\cert\loan;

use app\common\controller\Backend;
use think\Db;
use fast\Random;

/**
 * 贷款跟进
 *
 * @icon fa fa-circle-o
 */
class Follow extends Backend
{
    
    /**
     * Follow模型对象
     * @var \app\admin\model\cert\loan\Follow
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\cert\loan\Follow;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        
        $item_id = $this->request->request('item_id');
        $tmpwhere='1=1';
        if(!empty($item_id)){
            $tmpwhere .= " AND item_id='". $item_id ."'";
        }

        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['admin','certloanstate'])
                    ->where($where)
                    ->where($tmpwhere)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['admin','certloanstate'])
                    ->where($where)
                    ->where($tmpwhere)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('admin')->visible(['nickname']);
				$row->getRelation('certloanstate')->visible(['state_name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
      /**
     * 添加
     */
    public function add()
    {
        $item_id = $this->request->request('item_id');
        $state   = $this->request->request('state');
        $this->view->assign('item_id',$item_id);
        $this->view->assign('state',  $state);

        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $params['create_userid'] = $this->auth->id;
                    $params['create_time']  = date('Y-m-d h:m:s');
                    $params['create_ip']    = $this->request->ip();
                    $params['follow_id']    = str_replace(',','', Random::uuid());

                    $result = $this->model->allowField(true)->save($params);

                    // 更改主表信息
                    $loanModel = \app\admin\model\cert\loan\Items::get($item_id);
                    $loanModel->state = $params['state'];
                    $loanModel->last_time = date('Y-m-d h:m:s');
                    $loanModel->last_userid = $this->auth->id;
                    $loanModel->last_ip     = $this->request->ip();
                    $loanModel->save();
                    // rows
                    
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $stateList = \app\admin\model\cert\loan\State::all();
        $this->view->assign('stateList',$stateList);
        return $this->view->fetch();
    }
}
