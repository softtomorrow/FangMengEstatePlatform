<?php

namespace app\admin\model\newhouse\building;

use think\Model;


class Pictures extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'newhouse_building_pictures';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'lasttime_text'
    ];
    

    



    public function getLasttimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['lasttime']) ? $data['lasttime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setLasttimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function newhousebuildingitems()
    {
        return $this->belongsTo('app\admin\model\newhouse\building\Items', 'building_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function newhousebuildingpicturetype()
    {
        return $this->belongsTo('app\admin\model\newhouse\building\Picturetype', 'typeid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
