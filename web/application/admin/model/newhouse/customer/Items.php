<?php

namespace app\admin\model\newhouse\customer;

use think\Model;


class Items extends Model
{
    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'newhouse_customer_items';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'sex_radio_text',
        'marriage_radio_text'
    ];
    

    
    public function getSexRadioList()
    {
        return ['男' => __('男'), '女' => __('女'), '未知' => __('未知')];
    }

    public function getMarriageRadioList()
    {
        return ['未知' => __('未知'), '未婚' => __('未婚'), '已婚' => __('已婚'), '离异' => __('离异'), '丧偶' => __('丧偶')];
    }
    public function getFirstHouseRadioList()
    {
        return ['是' => __('是'), '否' => __('否'), '未知' => __('未知')];
    }
    public function getSocialSecurityRadioList()
    {
        return ['有' => __('有'), '无' => __('无'), '未知' => __('未知')];
    }
    public function getPaymentMethodRadioList()
    {
        return ['现金' => __('现金'), '刷卡' => __('刷卡'), '支付宝' => __('支付宝'), '微信' => __('微信')];
    }


    public function getSexRadioTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex_radio']) ? $data['sex_radio'] : '');
        $list = $this->getSexRadioList();
        return isset($list[$value]) ? $list[$value] : '';
    }
    public function getStateList()
    {
        return ['无效' => __('无效'), '带看' => __('带看'), '观望' => __('观望'), '已购' => __('已购')];
    }


    public function getStateTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['state']) ? $data['state'] : '');
        $list = $this->getStateList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getMarriageRadioTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['marriage_radio']) ? $data['marriage_radio'] : '');
        $list = $this->getMarriageRadioList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getFirstHouseTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['first_house']) ? $data['first_house'] : '');
        $list = $this->getFirstHouseRadioList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getSocialSecurityRadioTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['social_security']) ? $data['social_security'] : '');
        $list = $this->getSocialSecurityRadioList();
        return isset($list[$value]) ? $list[$value] : '';
    }

   

    public function getPaymentMethodRadioTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['payment_method']) ? $data['payment_method'] : '');
        $list = $this->getPaymentMethodRadioList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', ['admin'], 'LEFT')->setEagerlyType(0);
    }

}
