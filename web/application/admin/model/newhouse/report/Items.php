<?php
namespace app\admin\model\newhouse\report;
use think\Model;

class Items extends Model
{
    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'newhouse_report_items';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
    ];

    public function newhousecustomer()
    {
        return $this->belongsTo('app\admin\model\newhouse\customer\Items', 'customer_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function newhousebuilding()
    {
        return $this->belongsTo('app\admin\model\newhouse\building\Items', 'building_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function newhousecreateadmin(){
        return $this->belongsTo('app\admin\model\Admin','create_userid','id',[],'LEFT')->setEagerlyType(0);
    }
    public function newhouseverifyadmin(){
        return $this->belongsTo('app\admin\model\Admin','verify_userid','id',[],'LEFT')->setEagerlyType(0);
    }
}
