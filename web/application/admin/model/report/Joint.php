<?php

namespace app\admin\model\report;

use think\Model;


class Joint extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'newhouse_report_joint';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'cratetime_text',
        'relationship_text'
    ];
    

    
    public function getRelationshipList()
    {
        return ['未知' => __('未知'), '夫妻' => __('夫妻'), '父母' => __('父母'), '子女' => __('子女'), '亲戚' => __('亲戚'), '朋友' => __('朋友')];
    }


    public function getCratetimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['cratetime']) ? $data['cratetime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getRelationshipTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['relationship']) ? $data['relationship'] : '');
        $list = $this->getRelationshipList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setCratetimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function newhousereportitems()
    {
        return $this->belongsTo('app\admin\model\newhouse\report\Items', 'report_item_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
