<?php

namespace app\admin\model\rent;

use think\Model;


class Follow extends Model
{

    

    

    // 表名
    protected $name = 'rent_follow';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    

    public function getStateList()
    {
        return [ __('在租'), __('停租'), __('已租'),  __('无效')];
    }







    public function rentitems()
    {
        return $this->belongsTo('app\admin\model\rent\Items', 'item_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
