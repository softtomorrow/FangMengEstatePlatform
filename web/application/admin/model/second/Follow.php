<?php

namespace app\admin\model\second;

use think\Model;


class Follow extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'second_follow';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    public function getStateList()
    {
        return [ __('在售'), __('停售'), __('已售'),  __('无效')];
    }







    public function seconditems()
    {
        return $this->belongsTo('app\admin\model\second\Items', 'item_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
