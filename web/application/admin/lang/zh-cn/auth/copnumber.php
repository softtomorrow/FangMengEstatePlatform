<?php

return [
    'Id'                        => '编号',
    'Cop_id'                    => '公司',
    'Tablename'                 => '哪个表使用',
    'Year'                      => '年份',
    'Max_number'                => '当前编号',
    'Authcops.cop_name'         => '企业名称',
    'Authcops.address'          => '公司地址',
    'Authcops.areacode'         => '区域码',
    'Authcops.stamp_path'       => '印章地址',
    'Authcops.business_license' => '营业执照',
    'Authcops.certificate'      => '资质证书',
    'Authcops.state'            => '是否有效 0无效 1有效'
];
