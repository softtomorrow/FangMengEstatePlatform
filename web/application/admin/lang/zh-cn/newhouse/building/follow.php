<?php

return [
    'Id'                                  => '跟进编号',
    'Building_id'                         => '楼盘编号',
    'Create_userid'                       => '跟进人',
    'Create_time'                         => '跟进时间',
    'State'                               => '楼盘状态',
    'Comment'                             => '跟进内容',
    'Newhousebuildingitems.building_name' => '楼盘名称',
    'Admin.username'                      => '用户名'
];
