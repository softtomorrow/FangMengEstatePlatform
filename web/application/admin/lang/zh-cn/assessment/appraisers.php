<?php

return [
    'Cop_id'            => '公司id',
    'Add_userid'        => '录入人',
    'Createtime'        => '录入时间',
    'Create_ip'         => '录入IP',
    'Last_userid'       => '修改人',
    'Lasttime'          => '修改时间',
    'Last_ip'           => '修改IP',
    'Appraiser_name'    => '评估师姓名',
    'Licence_no'        => '证书编号',
    'Mobile'            => '联系方式',
    'Licence_image'     => '证书路径',
    'Authcops.cop_name' => '企业名称'
];
