<?php

return [
    'Title'                  => '标题',
    'cover'                  => '封面',
    'content'                => '内容',
    'display'                => '推荐',
    'hide'                   => '取消',

    'Release time'           => '发布时间',
    'operation'              => '操作',
];
