<?php

return [
    'Title'                  => '标题',
    'link'                   => '链接',
    'picture'                => '图片',
    'display'                => '显示',
    'hide'                   => '隐藏',

    'Release time'           => '发布时间',
    'operation'              => '操作',
];
