<?php

return [
    'Fav_id'        => '主键',
    'Cop_id'        => '所属公司',
    'User_id'       => '用户id',
    'Create_time'   => '收藏时间',
    'Create_ip'     => '创建IP',
    'Target_id'     => '主键',
    'Comment'       => '备注',
    'User.nickname' => '昵称'
];
