<?php

return [
    'Follow_id'                => '跟进编号',
    'Comment'                 => '跟进内容',
    'State'                    => '跟进状态',
    'Item_id'                   => '合同',
    'Create_time'               => '创建时间',
    'Create_ip'                 => '创建IP',
    'Admin.nickname'           => '昵称',
    'Certloanstate.state_name' => '贷款状态',
    'Create_userid'=>'跟进人'
];
