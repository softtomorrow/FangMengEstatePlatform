<?php
namespace app\index\controller;

use app\common\controller\Frontend;

class ReportItems extends Frontend
{

    protected $noNeedLogin = '';
    protected $noNeedRight = '*';

    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\newhouse\report\Items;


    }
    public function add(){
        $params = $this->request->post("row/a");
        if ($params) {
            // 是否已存在
            $exists = \app\admin\model\newhouse\report\Items::get(['customer_id'=>$params['customer_id'],'building_id'=>$params['building_id']]);
            if(!empty($exists)){
                $this->error("客户已经报备此楼盘！");                
            }else{
                $params['create_userid'] = $this->auth->id;
                $params['update_userid'] = $this->auth->id;
                $result = $this->model->allowField(true)->save($params);
                // 客户信息的报备楼盘字段同步修改
                $model_customer = \app\admin\model\newhouse\customer\Items::get($params['customer_id']);
                $array=explode(',',$model_customer->building_id);
                if(!in_array($params['building_id']))
                    $array[] = $params['building_id'];
                $model_customer->building_id = implode(',',$array);
                $model_customer->save();
                $this->success('报备操作成功，请等待审核。');
            }
        }
    }
}
