<?php

namespace app\index\controller;

use think\Db;
use app\common\controller\Frontend;

class Second extends Frontend
{

    protected $noNeedLogin = '';
    protected $noNeedRight = '*';
    protected $selectpageFields = '*';
    
    
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\common\model\SecondItems;
        $this->view->assign("stateList", $this->model->getStateList());
    }
    /**
     * 二手房源列表
     */
    public function seconditemslist(){
        $this->assign('title','二手房源列表');
        return $this->view->fetch();
    }
    /**
     * 查看二手房源详情
     */
    public function seconditemview($ids){
        
        $list=$this->model
            ->with(['seconddecoration','newhousebuildingtypes','authcops','admin','newhousebuilding'])
            ->where(['mf_second_items.id'=>$ids])
            ->select();
        $row=$list[0];

        if (!$row) {
            $this->error(__('No Results were found'));
        }
      
        $fav=\app\common\model\UserFavorate::get(['user_id'=>$this->auth->id,'target'=>'二手房出售房源','target_id'=>$ids]);
        $row->isFavorate=(!!$fav);
        
        // 权限
        if($row->view_level==3){
        // 个人私房
        if($row->add_userid!=$this->auth->id){
            $row->mobile='私房(无权限查看号码)';
            $row->dong='';
            $row->shi='';
        }
        }else if($row->view_level==2){
            //TODO: 本门店
        }else if($row->view_level==1){
            // 本公司
            if($row->add_cop_id!=$this->auth->cop_id){
                $row->mobile='私房(无权限查看号码)';
                $row->dong='';
                $row->shi='';
            }
        }
        $creator = \app\admin\model\Admin::get($row->add_userid);
        if(empty($creator)){
            $creatorName='';
        }else{
            $creatorName = $creator->nickname;
        }
        $createCop = \app\admin\model\auth\Cops::get($row->add_cop_id);
        if(empty($createCop)){
            $copName = '';
        }else{
            $copName = $creator->cop_name;
        }
        $this->view->assign("row",$row);
        $this->view->assign('creatorName',$creatorName);
        $this->view->assign('copName',$copName);

        return $this->view->fetch();
    }
    /**
     * 收藏房源列表
     */
    public function secondfavoratelist(){
        $this->assign('title','我的收藏');
        return $this->view->fetch();
    }
   
}