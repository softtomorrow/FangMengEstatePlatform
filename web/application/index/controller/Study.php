<?php

namespace app\index\controller;

use app\common\controller\Frontend;

class Study extends Frontend
{

    protected $noNeedLogin = ['index','book2'];
    protected $noNeedRight = '*';
    protected $layout = 'win';

    public function book2(){
       
        return $this->view->fetch();
    }
}
